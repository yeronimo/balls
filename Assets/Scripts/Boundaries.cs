﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boundaries : MonoBehaviour
{
    [SerializeField]
    private float thickness = 1f;
    [SerializeField]
    private float zScaleMultiplier = 5f;
    [SerializeField]
    private float zPosition = 490f;
    [SerializeField]
    private bool generateBoundaries = true;

    private Vector3 cameraPos;
    private Vector2 screenSize;

    private Dictionary<string, Transform> colliders;

    void Start()
    {
        if (!generateBoundaries)
            return;

        //Generate world space point information for position and scale calculations
        cameraPos = Camera.main.transform.position;
        screenSize.x = Vector2.Distance(Camera.main.ScreenToWorldPoint(new Vector2(0, 0)), Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, 0))) * 0.5f;
        screenSize.y = Vector2.Distance(Camera.main.ScreenToWorldPoint(new Vector2(0, 0)), Camera.main.ScreenToWorldPoint(new Vector2(0, Screen.height))) * 0.5f;

        GenerateBoundaries();
    }

    private void GenerateBoundaries()
    {
        colliders = new Dictionary<string, Transform>();

        colliders.Add("Top", new GameObject().transform);
        colliders.Add("Bottom", new GameObject().transform);
        colliders.Add("Right", new GameObject().transform);
        colliders.Add("Left", new GameObject().transform);

        foreach (KeyValuePair<string, Transform> c in colliders)
        {
            c.Value.gameObject.AddComponent<BoxCollider>();
            c.Value.name = c.Key + "Collider";
            c.Value.parent = transform;

            if (c.Key == "Left" || c.Key == "Right")
                c.Value.localScale = new Vector3(thickness, screenSize.y * 2, thickness * zScaleMultiplier);
            else
                c.Value.localScale = new Vector3(screenSize.x * 2, thickness, thickness * zScaleMultiplier);
        }

        colliders["Right"].position = new Vector3(cameraPos.x + screenSize.x + (colliders["Right"].localScale.x * 0.5f), cameraPos.y, zPosition);
        colliders["Left"].position = new Vector3(cameraPos.x - screenSize.x - (colliders["Left"].localScale.x * 0.5f), cameraPos.y, zPosition);
        colliders["Top"].position = new Vector3(cameraPos.x, cameraPos.y + screenSize.y + (colliders["Top"].localScale.y * 0.5f), zPosition);
        colliders["Bottom"].position = new Vector3(cameraPos.x, cameraPos.y - screenSize.y - (colliders["Bottom"].localScale.y * 0.5f), zPosition);
    }
}
