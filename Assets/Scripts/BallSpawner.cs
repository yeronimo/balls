﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BallSpawner : MonoBehaviour
{
    public static BallSpawner instance;

    public float defaultBallMass = 1f;
    public float defaultBallFieldRadius = 5f;
    /// <summary> Pool of objects for instantiated ball prefabs </summary>
    private List<GameObject> ballPool;
    [SerializeField]
    private GameObject ballPrefab;

    [SerializeField]
    [Tooltip("Total quantity balls to spawn. If it will be reached every ball will push back others and wont merge again.")]
    private int maxBallsCount; 

    [SerializeField]
    [Tooltip("Text counter")]
    private Text spawnedCounterText;

    [SerializeField]
    [Tooltip("Time between balls spawns in seconds.")]
    private float timeBetweenSpawn;

    /// <summary> Counter of spawned balls </summary>
    private int spawnedCounter;

    private void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        InitPool();
        StartCoroutine(SpawnCoroutine());
    }

    private void InitPool()
    {
        ballPool = new List<GameObject>();
    }
    /// <summary>
    /// Returns ball gameobject from objects pool if exists, otherwise adding new one to the pool.
    /// </summary>
    /// <returns>Ball gameobject.</returns>
    public GameObject GetBallFromPool()
    {
        GameObject ball = null;
        for (int i = 0; i < ballPool.Count; i++)
        {
            if (!ballPool[i].activeInHierarchy)
            {
                ball = ballPool[i];
            }
        }
        if (ball == null)
            ball = SpawnDefaultBall();
        return ball;

    }
    /// <summary>
    /// Instantiate ball prefab and add to pool.
    /// </summary>
    /// <returns>Instantiated ball gameobject<./returns>
    private GameObject SpawnDefaultBall()
    {
        if (maxBallsCount <= spawnedCounter)
            return null;
        GameObject go = Instantiate(ballPrefab, RandomizeSpawnPos(), Quaternion.identity);
        ballPool.Add(go);
        go.SetActive(false);
        return go;
    }

    private Vector3 RandomizeSpawnPos()
    {
        return Camera.main.ScreenToWorldPoint(new Vector3(Random.Range(Screen.width*.25f, Screen.width * .75f), Random.Range(Screen.height * .25f, Screen.height * .75f), Camera.main.farClipPlane / 2));
    }
    /// <summary>
    /// Ball spawning method used in StartCoroutine.
    /// </summary>
    /// <returns></returns>
    private IEnumerator SpawnCoroutine()
    {
        while (spawnedCounter < maxBallsCount)
        {
            GetBallFromPool().SetActive(true);
            SetSpawnedCounter();
            yield return new WaitForSeconds(timeBetweenSpawn);
        }

        foreach (GameObject ball in ballPool)
        {
            ball.GetComponent<ForceFieldBall>().ReverseFieldDirection();
        }
    }
    /// <summary>
    /// Updates quanity of spawned balls
    /// </summary>
    public void SetSpawnedCounter()
    {
        spawnedCounter++;
        SetSpawnedCountText();
    }

    private void SetSpawnedCountText()
    {
        spawnedCounterText.text = "Spawned balls: " + spawnedCounter.ToString();
    }
}
