﻿using UnityEngine;

public class ForceFieldBall : MonoBehaviour
{
    /// <summary> Is current ball merging with other one. </summary>
    public bool IsMerging;
    /// <summary> Quantity of merged balls with this one. Default is 1.</summary>
    public int MergedBalls { get; set; }
    /// <summary> Multiplier which indicates rather forcefield pushing or pulling back.</summary>
    public int FieldDirection { get; set; }

    [SerializeField]
    private LayerMask layerMask;
    [SerializeField]
    [Tooltip("Speed of created ball after bigger one explosion")]
    private float explodeSpeed;
    [SerializeField]
    [Tooltip("Quantity of default balls needed to calculate mass treshold for one ball")]
    private int massTresholdMultiplier = 5;

    private Collider m_collider;
    private Rigidbody m_rb;
    /// <summary>Array of colliders within ball force field range</summary>
    private Collider[] fieldColliders;
    private float forceFieldRadius;
    /// <summary>Ball mass threshold which exceed, ball will explode.</summary>
    private float massTreshold;
    private const float G = 6.67f;
    private bool canMerge;
    private bool attract;


    private void Awake()
    {
        m_rb = GetComponent<Rigidbody>();
        m_collider = GetComponent<Collider>();
    }

    private void OnEnable()
    {
        attract = true;
        IsMerging = false;
        m_rb.mass = BallSpawner.instance.defaultBallMass;
        forceFieldRadius = BallSpawner.instance.defaultBallFieldRadius;
        transform.localScale = Vector3.one/2;
        MergedBalls = 1;
    }

    // Start is called before the first frame update
    void Start()
    {
        m_rb.mass = BallSpawner.instance.defaultBallMass;
        forceFieldRadius = BallSpawner.instance.defaultBallFieldRadius;
        massTreshold = BallSpawner.instance.defaultBallMass * massTresholdMultiplier;
       
        FieldDirection = 1;
        canMerge = true;
    }

    void FixedUpdate()
    {
        if (attract)
            ForceFieldAttract();
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.layer == 9 && canMerge)
        {
            if (!IsMerging && !collision.gameObject.GetComponent<ForceFieldBall>().IsMerging)
            {
                MergeOnCollision(collision.gameObject);
            }
        }
    }

    private void EnableCollider()
    {
        m_collider.enabled = true;
    }

    private void DisableCollider()
    {
        m_collider.enabled = false;
    }

    //F = Gm1m2/r2
    private void ForceFieldAttract()
    {
        fieldColliders = Physics.OverlapSphere(transform.position, forceFieldRadius, layerMask);
        Rigidbody rb;
        Vector3 dir;
        float dist;
        float force;

        foreach (Collider c in fieldColliders)
        {
            if (c != this)
            {
                rb = c.gameObject.GetComponent<Rigidbody>();
                dir = m_rb.position - rb.position;
                dist = dir.magnitude;
                force = G * m_rb.mass * rb.mass / dist * dist;
                //print(force);
                if (!float.IsNaN(force))
                    rb.AddForce(force * dir.normalized * FieldDirection);
            }
        }
    }
    
    private void MergeOnCollision(GameObject go)
    {
        Rigidbody rb = go.GetComponent<Rigidbody>();
        ForceFieldBall fieldBall = go.GetComponent<ForceFieldBall>();
        IsMerging = true;
        fieldBall.IsMerging = true;

        if (m_rb.mass >= rb.mass)
        {
            transform.localScale += go.transform.localScale;
            go.SetActive(false);
            forceFieldRadius++;
            m_rb.mass += rb.mass;
            MergedBalls += fieldBall.MergedBalls;

            if (CheckMassTreshold())
            {
                OnMassExceeded();
                return;
            }

            IsMerging = false;
        }
    }
    
    private bool CheckMassTreshold()
    {
        return m_rb.mass >= massTreshold;
    }

    private void OnMassExceeded()
    {
        attract = false;
        BallExplode();
        gameObject.SetActive(false);
    }

    private void BallExplode()
    {
        GameObject smallerBall;
        for (int i = 0; i < MergedBalls; i++)
        {
            smallerBall = BallSpawner.instance.GetBallFromPool();
            if (smallerBall == null)
                return;

            smallerBall.SetActive(true);
            smallerBall.transform.position = transform.position;
            smallerBall.GetComponent<ForceFieldBall>().SetRandomForce();
            BallSpawner.instance.SetSpawnedCounter();
        }
    }

    public void SetRandomForce()
    {
        DisableCollider();
        Invoke("EnableCollider", .5f);
        m_rb.AddRelativeForce(Random.onUnitSphere * explodeSpeed);
    }
   
    public void ReverseFieldDirection()
    {
        FieldDirection = -1;
        canMerge = false;
    }
}
